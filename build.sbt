lazy val commonSettings = Seq(
  organization := "io.artfuldodge",
  scalaVersion := "2.12.4",
  scalacOptions ++= Seq(
    "-target:jvm-1.8", // Force compile with specified java version.
    "-deprecation", // Emit warning and location for usages of deprecated APIs.
    "-encoding",
    "utf-8", // Specify character encoding used by source files.
    "-Xlint"
  )
)


lazy val root: Project = project
  .in(file("."))
  .settings(commonSettings)
  .settings(
    PB.targets in Compile := Seq(scalapb.gen(flatPackage = true) -> (sourceManaged in Compile).value),
    libraryDependencies ++= Seq(
      "ch.qos.logback"             % "logback-classic"       % "1.2.3",
      // "com.google.auth"            % "google-auth-library-credentials" % "0.9.0",
      "com.google.auth"            % "google-auth-library-oauth2-http" % "0.4.0" exclude("com.google.guava", "guava-jdk5"),
      "com.trueaccord.scalapb"     %% "scalapb-runtime"      % com.trueaccord.scalapb.compiler.Version.scalapbVersion % "protobuf",
      "com.trueaccord.scalapb"     %% "scalapb-runtime-grpc" % com.trueaccord.scalapb.compiler.Version.scalapbVersion,
      "io.grpc"                    % "grpc-netty"            % com.trueaccord.scalapb.compiler.Version.grpcJavaVersion,
      "io.grpc"                    % "grpc-auth"             % com.trueaccord.scalapb.compiler.Version.grpcJavaVersion,
      "io.netty"                   % "netty-codec-http2"     % "4.1.16.Final",
      "io.netty"                   % "netty-tcnative-boringssl-static" % "2.0.6.Final",
      "org.isomorphism"            % "token-bucket"          % "1.6"
    )
  )

