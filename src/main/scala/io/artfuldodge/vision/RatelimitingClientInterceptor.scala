package io.artfuldodge.vision

import io.grpc.{
  CallOptions,
  ClientCall,
  ClientInterceptor,
  Channel,
  Context,
  ForwardingClientCall,
  MethodDescriptor
}
import org.isomorphism.util.TokenBucket
import java.util.concurrent.Executors
import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.concurrent.duration.DurationInt
import scala.util.control.{ Exception, NonFatal }


class RatelimitingClientInteceptor(bucketSupplier: () => TokenBucket) extends ClientInterceptor {
  val executor = Executors.newSingleThreadExecutor
  implicit val ec = ExecutionContext.fromExecutorService(executor)
  val bucket = Await.result(Future(bucketSupplier()), 1.second)
  val capacity = bucket.getCapacity.toInt

  def interceptCall[Req, Rep](method: MethodDescriptor[Req, Rep], options: CallOptions, next: Channel): ClientCall[Req, Rep] = {
    new ForwardingClientCall.SimpleForwardingClientCall[Req, Rep](next.newCall(method, options)) {

      override def request(messages: Int): Unit = {
        val context = Context.current()
        Future {
          val prev = context.attach()
          Exception.ultimately(context.detach(prev)) {
            if (messages <= capacity) {
              bucket.consume(messages)
              delegate.request(messages)
            } else {
              bucket.consume(capacity)
              delegate.request(capacity)
              Future(request(messages - capacity))
            }
          }
        }
      }
    }
  }

  def shutdown() = executor.shutdown
}
