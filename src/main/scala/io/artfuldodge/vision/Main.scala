package io.artfuldodge.vision
 
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.vision.v1.ImageAnnotatorGrpc
import com.google.cloud.vision.v1.BatchAnnotateImagesRequest
import java.util.concurrent.TimeUnit
import io.grpc.auth.MoreCallCredentials
import org.isomorphism.util.TokenBuckets
import scala.concurrent.{ Future, Await }
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

object Main extends App {

  val creds = GoogleCredentials.getApplicationDefault()
  val callCreds = MoreCallCredentials.from(creds)
  val limiter = new RatelimitingClientInteceptor({ () =>
    TokenBuckets.builder()
      .withCapacity(1)
      .withFixedIntervalRefillStrategy(1, 1, TimeUnit.SECONDS)
      .build();
  })

  val channel = io.grpc.ManagedChannelBuilder.forAddress("vision.googleapis.com", 443).build()

  val stub =
    ImageAnnotatorGrpc.stub(channel)
      .withCallCredentials(callCreds)
      .withInterceptors(limiter)

  val res = Future.traverse(Seq(0,1,2,3,4,5,6,7,8,9)){ _ =>
    val req = BatchAnnotateImagesRequest()
    stub.batchAnnotateImages(req)
  }

  println(Await.result(res, 10.minutes))
  limiter.shutdown()
  channel.shutdown()
}
